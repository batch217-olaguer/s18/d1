// console.log("Hello World");

// [SECTION] Functions
// Parameters and Arguments

/*function printInput() {
	let nickName = prompt("Enter your nickname:");
	console.log("Hi, " +nickName+ "!");
}
printInput();*/

// This function has a parameter and argument
function printName(name /*<--- this is the parameter*/){
	console.log("My name is " +name);
}
 printName("Juana"/* <-- this is an argument.*/);

// 1. printName("Juana"); --> ARGUMENT
// 2. argument will be the value of our parameter.

let miniActivity1 = function printName1(name1){
	console.log("My name is " +name1 +".");
}
miniActivity1("aL");

// Function arguments cannot be used by a function if there are no parameters provided within the function.
function checkDivisibilityBy8(num){
	let remainder = num%8
	console.log("The remainder of " +num+ " divided by 8 is: " +remainder)
	let isDivisibleBy8 = remainder === 0
	console.log("Is " +num+ " divisible by 8?");
	console.log(isDivisibleBy8);
}
checkDivisibilityBy8(8);

// you cn also do the same using prompt(), however prompt outputs a string, which is not ideal for mathematical computations

// functions as arguments
// function parameters can also accept other functions as arguments.
function argumentFunction(){
	console.log("this function was passed as an argument before the message.");
}

function invokeFunction(argumentFunction)/*you can reuse invokeFunction with different parameters*/{
	argumentFunction();
}
invokeFunction(argumentFunction);

// Multiple Parameters
function createFullName(firstName, middleName, lastName){
	console.log(firstName+ ' ' +middleName+ ' ' +lastName);
}
createFullName("Al Victor", "Mendiola", "Olaguer");
createFullName("Al Victor", "Mendiola", "");

// Multiple parameter using stored data in a variable
let firstName = "John";
let middleName = "Doe";
let lastName = "Smith";
createFullName(firstName, middleName, lastName);

// Parameter names are just names to refer to the argument. Even if we change the name of the parameters, the arguments will be received in the same order it was passed.

function printFullName(middleName, firstName, lastName){
	console.log(firstName+ ' ' +middleName+ ' ' +lastName);
}
printFullName("Juan", "Dela", "Cruz"); /*this follows the order in which the parameter was declared. Juan -> middle name, dela-> first name, cruz -> last name.*/

// [SECTION] Return statement
// The "return" statement allows us to output a value from a function to be passed to the line/block of code that invoked/called the function.

function returnFullName(firstName, middleName, lastName){
	console.log("Test console");
	return firstName+ ' ' +middleName+ ' ' +lastName;
	console.log("this will not be listed in the console.");
}

returnFullName("Juan", "Dela", "Cruz");
let completeName = returnFullName("Asiong", "Tagailog", "Salongga");
console.log(completeName);
console.log(returnFullName(firstName, middleName, lastName));

// you can also create a variable inside the function to contain the result and return that variable instead.

function returnAddress(city, country){
	let fullAddress = city+ ", " +country;
	return fullAddress;
	console.log("This will not be displayed");
}

let myAddress = returnAddress("Cebu City", "Philippines");
console.log(myAddress);

// On the other hand

function printPlayerInfo(username, level, job){
/*	console.log("Username: " +username);
	console.log("Level: " +level);
	console.log("Job; " +job);*/
	return "Username: " +username;
}

let user1 = printPlayerInfo("Knight_white", 95, "Paladin");
console.log(user1);
console.log(username);